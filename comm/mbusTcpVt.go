package comm

import (
	"encoding/binary"
	"io"
	"log/slog"
	"net"
	"sync/atomic"
	"time"
)

// 返回值按照 modbusTcp协议进行解析的 虚拟机

type MbusTcpVT struct {
	Key         string //基站编码
	Ipaddr      string
	Conn        net.Conn     //与服务器的链路
	EventChan   chan *[]byte //读管道
	wChan       chan []byte  //写管道
	payload     []byte
	decoderFlag int
	connected   atomic.Bool
	tms         atomic.Int64
}

func NewMbusTcpVT(key string, ipaddr string, decoderFlag int) *MbusTcpVT {
	vt := &MbusTcpVT{
		Key:         key,
		Ipaddr:      ipaddr,
		EventChan:   make(chan *[]byte, 10),
		wChan:       make(chan []byte, 20),
		payload:     make([]byte, 0, 512),
		decoderFlag: decoderFlag,
	}
	return vt
}

func (v *MbusTcpVT) ConnectServer() bool {
	v.tms.Store(time.Now().UnixMilli())
	if v.connected.Load() {
		return true
	}
	conn, err := net.Dial("tcp", v.Ipaddr)
	if err != nil {
		slog.Error("MbusTcpVT,ConnectServer", "err", err)
		return false
	}
	slog.Info("终端连接成功！", "code", v.Key)
	v.Conn = conn
	v.connected.Store(true)
	go v.readConn()
	go v.writeConn()
	go v.checkReadTimeout()
	return true
}

func (v *MbusTcpVT) IsConnected() bool {
	return v.connected.Load()
}

func (v *MbusTcpVT) Close() {
	v.Conn.Close()
	slog.Info("虚拟终端退出！", "code", v.Key)
}

func (v *MbusTcpVT) Write(data []byte) bool {
	if v.IsConnected() {
		v.wChan <- data
		return true
	}
	return false
}
func (v *MbusTcpVT) readConn() {
	for {

		if !v.connected.Load() {
			break
		}
		buff := make([]byte, 512)
		len, err := v.Conn.Read(buff)
		// 收到的数据长度为0
		if err != nil {
			if err != io.EOF {
				v.connected.Store(false)
				slog.Error("readConn 断开连接！", "err", err)
				slog.Info("readConn 断开连接！", "code", v.Key, "收到的数据长度为", len)
				break
			}
		} else {
			v.tms.Store(time.Now().UnixMilli())
			// fmt.Println(buff[:len])
			if v.decoderFlag == 0 {
				v.decodeTcpData(buff[:len])
			} else {
				v.decodeRtuData(buff[:len])
			}

		}
	}

}

func (v *MbusTcpVT) initPayload() {
	v.payload = make([]byte, 0)
}

func (v *MbusTcpVT) decodeTcpData_one(data []byte) (bool, []byte, []byte) {
	if (len(data)) < 7 {
		return false, nil, data
	}
	ver := binary.BigEndian.Uint16(data[2:4])
	ln := binary.BigEndian.Uint16(data[4:6]) + 6
	dLn := len(data)
	// fmt.Println("一次解析：", ver, ln, dLn)
	if (ver == 0) && (int(ln) <= dLn) {
		return true, data[:ln], data[ln:]
	}
	return false, nil, data
}

func (v *MbusTcpVT) decodeTcpData(data []byte) {
	// fmt.Println(v.Key, "收到的数据：", data)
	//解析data数据，是否是有效数据
	b, ne, rst := v.decodeTcpData_one(data)

	if b {
		v.EventChan <- &ne
		v.initPayload()
		if len(rst) > 0 {
			v.decodeTcpData(rst)
		}
	} else {
		v.payload = append(v.payload, data...)
		b1, ne1, _ := v.decodeTcpData_one(v.payload)
		if b1 {
			v.EventChan <- &ne1
			v.initPayload()
		}
	}
}

func (v *MbusTcpVT) writeConn() {
	for {
		if !v.connected.Load() {
			break
		}
		d := <-v.wChan
		_, err := v.Conn.Write(d)
		if err != nil {
			v.connected.Store(false)
			slog.Info("writeConn 断开连接！", "code", v.Key)
			break
		}
	}
}

func (v *MbusTcpVT) Send(data []byte) {
	v.Write(data)
}

func (v *MbusTcpVT) decodeRtuData_one(data []byte) (bool, []byte, []byte) {
	//解析出的包长度
	ln := data[2] + 5
	dLn := len(data)
	// fmt.Println("一次解析：", ver, ln, dLn)
	if int(ln) <= dLn {
		return true, data[:ln], data[ln:]
	}
	return false, nil, data
}

func (v *MbusTcpVT) decodeRtuData(data []byte) {
	// fmt.Println(v.Key, "收到的数据：", data)
	//解析data数据，是否是有效数据
	b, ne, rst := v.decodeRtuData_one(data)

	if b {
		v.EventChan <- &ne
		v.initPayload()
		if len(rst) > 0 {
			v.decodeRtuData(rst)
		}
	} else {
		v.payload = append(v.payload, data...)
		b1, ne1, _ := v.decodeRtuData_one(v.payload)
		if b1 {
			v.EventChan <- &ne1
			v.initPayload()
		}
	}
}

func (v *MbusTcpVT) checkReadTimeout() {
	for {
		l := time.Now().UnixMilli() - v.tms.Load()
		if l > 5000 {
			v.connected.Store(false)
			slog.Info("checkReadTimeout 断开连接！", "code", v.Key, "时间差", l)
		}
		time.Sleep(1000 * time.Millisecond)
	}
}
