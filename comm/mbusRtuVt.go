package comm

import (
	"bytes"
	"encoding/binary"
	"log/slog"
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/sgems/sg.core/utils"
	"github.com/tarm/serial"
)

type MbusRtuVT struct {
	Key         string                           //基站编码
	comNo       string                           //串口号
	baud        int                              //波特率
	Conn        *serial.Port                     // 串口客户端
	EventChan   chan *utils.Tup2[[]byte, []byte] //读管道
	RChan       chan []byte                      //写管道
	WChan       chan []byte                      //写管道
	cfg         *serial.Config
	connected   atomic.Bool
	readBuffLen int //读缓冲区的长度
}

// 构造函数
func NewMbusRtuVT(key string, comNo string, baud int, buffLen int) *MbusRtuVT {
	vt := &MbusRtuVT{
		Key:         key,
		comNo:       comNo,
		baud:        baud,
		cfg:         &serial.Config{Name: comNo, Baud: baud, Parity: serial.ParityNone, StopBits: serial.Stop1},
		EventChan:   make(chan *utils.Tup2[[]byte, []byte], 10),
		RChan:       make(chan []byte, 10),
		WChan:       make(chan []byte, 10),
		readBuffLen: buffLen,
	}
	return vt
}

func (v *MbusRtuVT) ConnectServer() bool {
	if v.connected.Load() {
		return true
	}
	conn, err := serial.OpenPort(v.cfg)
	if err != nil {
		slog.Error("MbusRtuVT,ConnectServer", "err", err, v.cfg)
		return false
	}
	slog.Info("终端连接成功！", "code", v.Key)
	v.Conn = conn
	v.connected.Store(true)
	// go v.readConn()
	go v.writeConn()
	return true
}

func (v *MbusRtuVT) IsConnected() bool {
	return v.connected.Load()
}

func (v *MbusRtuVT) Close() {
	v.Conn.Close()
	slog.Info("虚拟终端退出！", "code", v.Key)
}

func (v *MbusRtuVT) readConnOne() {

	if !v.connected.Load() {
		return
	}
	buff := make([]byte, v.readBuffLen)
	len, err := v.Conn.Read(buff)
	// 收到的数据长度为0
	if len == 0 || err != nil {
		slog.Info("readConn 断开连接！", "code", v.Key, "收到的数据长度为", len)
		slog.Error("readConn 断开连接！", "err", err)
		v.connected.Store(false)

	} else {
		v.RChan <- buff[:len]
	}

}

func (v *MbusRtuVT) writeConn() {
	for {
		req := <-v.WChan
		if !v.connected.Load() {
			break
		}
		for i := 0; i < 3; i++ {
			_, err := v.Conn.Write(req)
			if err != nil {
				slog.Info("writeConn 断开连接！", "code", v.Key)
				v.connected.Store(false)
				break
			}

			v.readConnOne()

			//请求的包长度
			buff := bytes.NewBuffer(req)
			rq := &Req{}
			binary.Read(buff, binary.BigEndian, rq)

			d := <-v.RChan
			// slog.Info("收到数据===================", "req", utils.ToHexString(req), "reps", utils.ToHexString(d))
			b, rst := v.decodeData_one(rq, d)
			if b {
				slog.Info("收到数据===================", "循环了", i, "req", utils.ToHexString(req), "reps", utils.ToHexString(d))
				i = 100
				v.EventChan <- &utils.Tup2[[]byte, []byte]{A: req, B: rst}
			}
			time.Sleep(200 * time.Millisecond)
		}
	}
}

func (v *MbusRtuVT) Send(data []byte) {
	v.WChan <- data
}

func (v *MbusRtuVT) decode(req []byte, wg *sync.WaitGroup) {
	//请求的包长度
	buff := bytes.NewBuffer(req)
	rq := &Req{}
	binary.Read(buff, binary.BigEndian, rq)
	// data := make([]byte, 0)
	for {
		select {
		case d := <-v.RChan:
			slog.Info("收到数据===================", "req", utils.ToHexString(req), "reps", utils.ToHexString(d))

			// data = append(data, d...)
			b, rst := v.decodeData_one(rq, d)
			if b {
				v.EventChan <- &utils.Tup2[[]byte, []byte]{A: req, B: rst}
				wg.Done()
				return
			}
		case <-time.After(50 * time.Millisecond):
			slog.Info("超时===================", "code", v.Key)
			wg.Done()
			return
		}
	}
}

func (v *MbusRtuVT) decodeData_one(req *Req, data []byte) (bool, []byte) {
	if len(data) > 5 &&
		data[0] == req.Addr &&
		data[1] == req.Fn &&
		data[2] == byte(req.Val)*2 {
		//解析出的包长度
		ln := data[2] + 5
		dLn := len(data)
		// fmt.Println("一次解析：", ver, ln, dLn)
		if int(ln) <= dLn {
			// CRC校验
			if utils.ValidCrc16(data[:ln]) {
				return true, data[:ln]
			}
			slog.Info("CRC校验失败", "元数据", utils.ToHexString(data), utils.ToHexString(data[:ln]))
			return false, nil
		}
	}
	return false, nil
}
