package comm

import "time"

type Vt interface {
	ConnectServer() bool
	IsConnected() bool
	Close()
}

type SgVCity struct {
	Vts []Vt
}

var (
	Sgvcity *SgVCity
)

func init() {
	Sgvcity = &SgVCity{}
}

func (c *SgVCity) Regist(vt Vt) *SgVCity {
	c.Vts = append(c.Vts, vt)
	return c
}

func (c *SgVCity) Start() {
	go func() {
		for {
			for _, v := range c.Vts {
				if !v.IsConnected() {
					v.ConnectServer()
				}
			}
			time.Sleep(time.Second * time.Duration(2))
		}
	}()
}

func (c *SgVCity) Stop() {
	for _, v := range c.Vts {
		v.Close()
	}
}
