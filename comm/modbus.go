package comm

import (
	"bytes"
	"encoding/binary"
)

type (
	TcpReqHeader struct {
		Sn  uint16 //序列号
		Ver uint16 //版本
		Ln  uint16 //长度
	}

	// 0x03 0x04 的数据请求，和 0x06的数据设置
	Req struct {
		Addr byte   //设备地址
		Fn   byte   //功能码
		Pos  uint16 //寄存器地址
		Val  uint16 //内容
	}

	// 有符号的请求值
	ReqS struct {
		Addr byte   //设备地址
		Fn   byte   //功能码
		Pos  uint16 //寄存器地址
		Val  int16  //内容
	}

	Set10 struct {
		Addr byte   //设备地址
		Fn   byte   //功能码
		Pos  uint16 //寄存器地址
		Cnt  uint16 //寄存器个数
		Len  uint8  //后继数据长度
		Val  any    //内容
	}

	TcpRespHeader struct {
		Sn   uint16 //序列号
		Ver  uint16 //版本
		Ln   uint16 //长度
		Addr byte   //设备地址
		Fn   byte   //功能码
	}

	RtuRespHeader struct {
		Addr byte //设备地址
		Fn   byte //功能码
	}
)

func (c *Req) ToTcp() []byte {

	buff := &bytes.Buffer{}
	tcp := &TcpReqHeader{
		Sn:  c.Pos,
		Ver: 0,
		Ln:  6,
	}
	binary.Write(buff, binary.BigEndian, tcp)
	binary.Write(buff, binary.BigEndian, c)
	return buff.Bytes()
}

func (c *ReqS) ToTcp() []byte {

	buff := &bytes.Buffer{}
	tcp := &TcpReqHeader{
		Sn:  c.Pos,
		Ver: 0,
		Ln:  6,
	}
	binary.Write(buff, binary.BigEndian, tcp)
	binary.Write(buff, binary.BigEndian, c)
	return buff.Bytes()
}
func (c *ReqS) ToRtu() []byte {

	buff := &bytes.Buffer{}
	binary.Write(buff, binary.BigEndian, c)
	crc16(buff)
	return buff.Bytes()
}

func (c *Req) ToRtu() []byte {

	buff := &bytes.Buffer{}
	binary.Write(buff, binary.BigEndian, c)
	crc16(buff)
	return buff.Bytes()
}

func (c *Set10) ToTcp() []byte {

	buff := &bytes.Buffer{}
	tcp := &TcpReqHeader{
		Sn:  c.Pos,
		Ver: 0,
		Ln:  uint16(7 + c.Len),
	}
	binary.Write(buff, binary.BigEndian, tcp)
	binary.Write(buff, binary.BigEndian, c.Addr)
	binary.Write(buff, binary.BigEndian, c.Fn)
	binary.Write(buff, binary.BigEndian, c.Pos)
	binary.Write(buff, binary.BigEndian, c.Cnt)
	binary.Write(buff, binary.BigEndian, c.Len)
	binary.Write(buff, binary.BigEndian, c.Val)

	return buff.Bytes()
}

func crc16(buff *bytes.Buffer) {
	bytes := buff.Bytes()
	res := 0xFFFF
	for _, data := range bytes {
		res = res ^ int(data)
		for i := 0; i < 8; i++ {
			if (res & 0x0001) == 1 {
				res = (res >> 1) ^ 0xA001
			} else {
				res = res >> 1
			}
		}
	}
	buff.WriteByte(byte(res & 0xFF))
	buff.WriteByte(byte((res >> 8) & 0xFF))
}
func ParesRtuReq(d []byte) *Req {
	buff := bytes.NewBuffer(d)
	h := &Req{}
	binary.Read(buff, binary.BigEndian, h)
	return h
}
