package main

import (
	"fmt"
	"time"

	"gitee.com/sgems/sg.core/core"
	"gitee.com/sgems/sg.core/utils"
)

func main() {
	tt_kyp()
}

func tt_kyp() {
	ky := core.OfKyTrans("ss", 233, "ff")
	ky.
		PushBms(&core.BmsE{V: 1}).
		PushBms(&core.BmsE{V: 2})

	ss := utils.ToJsonString(ky)

	fmt.Println(ss)
}

func tt_SgAggr() {
	fmt.Println("sss")
	aggr_csv := utils.NewSgAggr[utils.Tup2[string, string]](1, time.Minute)
	go listen(aggr_csv)
	for {
		dt := time.Now().UnixMilli()
		aggr_csv.Add("cc", dt, utils.Tup2[string, string]{"a", "b"})
		time.Sleep(time.Second * 10)
	}

}

func listen(aggr_csv *utils.SgTimeAggr[utils.Tup2[string, string]]) {
	for data := range aggr_csv.EChan {
		fmt.Println("saveAggrData===========", time.Now(), data.K, data.V)
	}
}
