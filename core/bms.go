package core

import (
	"time"
)

const (
	//系统状态：0运行；1停机；2禁放；3禁充；4禁充放；5告警；6故障；7预警；
	BMS_STATUS_RUN  = 0
	BMS_STATUS_STOP = 1
	BMS_STATUS_DPF  = 2
	BMS_STATUS_DPC  = 3
	BMS_STATUS_DP   = 4
	BMS_STATUS_WARN = 5
	BMS_STATUS_ERR  = 6
	BMS_STATUS_WAR  = 7
)

// 需要进行业务计算的核心数据
type (
	// 主体进行业务计算的数据
	CBms struct {
		Dt    int64   `json:"dt"`
		No    int     `json:"no"`
		W_o   float64 `json:"w_o"` //放电功率
		W_i   float64 `json:"w_i"` //充电功率
		Soc   float64 `json:"soc"`
		V_max float64 `json:"v_max"`
		V_min float64 `json:"v_min"`
		T_max float64 `json:"t_max"`
		T_min float64 `json:"t_min"`
		Q_o   float64 `json:"q_o"` //日放电电量
		Q_i   float64 `json:"q_i"` //日充电电量
		Q0_o  float64
		Q0_i  float64
		Q1_o  float64
		Q1_i  float64
		Price float64
		Seg   int //时段，1尖 2峰 3平 4谷 5深谷

	}

	// 电池数据  主体进行业务计算的数据
	BmsE struct {
		Attm     string  `json:"attm"`
		Dt       int64   `json:"dt"`
		No       int     `json:"no"`
		V        float64 `json:"v"`
		C        float64 `json:"c"`
		Soc      float64 `json:"soc"`
		Soh      float64 `json:"soh"`
		V_max    float64 `json:"v_max"`
		V_max_p  string  `json:"v_max_p"` //最高电压电芯所在位置 1#-2-12
		V_min    float64 `json:"v_min"`
		V_min_p  string  `json:"v_min_p"` //最低电压电芯所在位置 1#-2-12
		T_max    float64 `json:"t_max"`
		T_max_p  string  `json:"t_max_p"` //最高温度电芯所在位置 1#-2-12
		T_min    float64 `json:"t_min"`
		T_min_p  string  `json:"t_min_p"` //最低温度电芯所在位置 1#-2-12
		Q_i_sum  float64 `json:"q_i_sum"` //堆累计充电电量
		Q_o_sum  float64 `json:"q_o_sum"`
		W_o_lim  float64 `json:"w_o_lim"`  //允许最大放电功率
		W_i_lim  float64 `json:"w_i_lim"`  //允许最大充电功率
		C_o_lim  float64 `json:"c_o_lim"`  //允许最大放电电流
		C_i_lim  float64 `json:"c_i_lim"`  //允许最大充电电流
		Q_i_lim  float64 `json:"q_i_lim"`  //可充电量
		Q_o_lim  float64 `json:"q_o_lim"`  //可放电量
		Status   int     `json:"status"`   //系统状态：0运行；1停机；2禁放；3禁充；4禁充放；5告警；6故障；7预警；
		Statusio int     `json:"statusio"` //充放电状态：0 睡眠 1放电 2充电
		I_rate   float64 `json:"i_rate"`   //BMS充电比例 告警降功率
		O_rate   float64 `json:"o_rate"`   //BMS放电比例 告警降功率
	}

	BmsCell struct {
		Dt    int64
		Attm  time.Time //
		No    int       //簇编号
		DType string    //数据类型 协能的含义：正极柱温度（T+），负极柱温度（T-） 单体电压（CV） 单体温度（CT）
		Cno   int       //数据起始电芯编号
		Vals  []float32 //电芯数据
	}
)
