package core

type StsSt struct {
	Dt     int64 `json:"dt"`
	Status int   `json:"status"` // 0无效 1并网 2离网
}

type StsE struct {
	Attm string `json:"attm"`
	Dt   int64  `json:"dt"`
	No   int    `json:"no"`
	//电网数据
	Hz_eg    float64 `json:"hz_eg"`    //频率(电网侧) 频率(电网侧)
	Wf_eg    float64 `json:"wf_eg"`    //总功率因数(电网侧)
	Lv_eg_ab float64 `json:"lv_eg_ab"` //AB线电压(电网侧)
	Lv_eg_bc float64 `json:"lv_eg_bc"` //BC线电压(电网侧)
	Lv_eg_ca float64 `json:"lv_eg_ca"` //CA线电压(电网侧)
	C_eg_a   float64 `json:"c_eg_a"`   //A相电流(电网侧)
	C_eg_b   float64 `json:"c_eg_b"`   //B相电流(电网侧)
	C_eg_c   float64 `json:"c_eg_c"`   //C相电流(电网侧)
	W1_eg    float64 `json:"w1_eg"`    //总有功功率(电网侧)
	W2_eg    float64 `json:"w2_eg"`    //总无功功率(电网侧)
	W3_eg    float64 `json:"w3_eg"`    //总视在功率(电网侧)
	//负载数据
	Hz_fz    float64 `json:"hz_fz"`    //频率(负载侧)
	Wf_fz    float64 `json:"wf_fz"`    //总功率因数(负载侧)
	Lv_fz_ab float64 `json:"lv_fz_ab"` //AB线电压(负载侧)
	Lv_fz_bc float64 `json:"lv_fz_bc"` //BC线电压(负载侧)
	Lv_fz_ca float64 `json:"lv_fz_ca"` //CA线电压(负载侧)
	C_fz_a   float64 `json:"c_fz_a"`   //A相电流(负载侧)
	C_fz_b   float64 `json:"c_fz_b"`   //B相电流(负载侧)
	C_fz_c   float64 `json:"c_fz_c"`   //C相电流(负载侧)
	W1_fz    float64 `json:"w1_fz"`    //总有功功率(负载侧)
	W2_fz    float64 `json:"w2_fz"`    //总无功功率(负载侧)
	W3_fz    float64 `json:"w3_fz"`    //总视在功率(负载侧)
}

// type StsE struct {
// 	Status int `json:"status"` // 0无效 1并网 2离网
// 	*Sts
// }
