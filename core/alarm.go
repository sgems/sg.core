package core

import (
	"time"

	"gitee.com/sgems/sg.core/utils"
)

//告警相关
//按厂家编制的告警
//告警码生成规则
//[前缀][设备厂家][设备序号][寄存器地址][告警位]
//例如，协能BMS堆故障的编码： D 02 0 0001 1
// 设备序号，取值范围 0到35

const (

	//1、前缀描述
	//B 二级架构的BMS
	AC_PREFIX_BMS_独 = "B"
	//D 三级架构的BMS的堆
	AC_PREFIX_BMS_堆 = "D"
	//C 三级架构的BMS的簇
	AC_PREFIX_BMS_簇 = "C"
	//W BMS的外设
	AC_PREFIX_BMS_外设 = "W"
	//P PCS
	AC_PREFIX_PCS = "P"
	//S STS
	AC_PREFIX_STS = "S"
	//M 光伏 MPPT
	AC_PREFIX_PV = "M"
	//E EMS产生的告警
	AC_PREFIX_EMS = "E"

	//2、厂家 两位，数字加字符
	AC_MANU_NONE = "00"
	AC_MANU_昆宇   = "KY"
	AC_MANU_高特   = "01"
	AC_MANU_协能   = "02"
	AC_MANU_盛宏   = "03"
	AC_MANU_禾望   = "04"
	AC_MANU_汇川   = "05"
	AC_MANU_科工   = "06"
	AC_MANU_睿能   = "07"
	AC_MANU_精石   = "08"
	AC_MANU_N9   = "N9"
)

type AlarmCode struct {
	prefix string //前缀
	manu   string //厂家
	no     int    //序号
	Codes  []string
}

type CAlarmE struct {
	Dt    int64  `json:"dt"`
	Acode string `json:"acode"`
	Count int    `json:"count"`
}

type AlarmE struct {
	Dt    int64  `json:"dt"`
	Acode string `json:"acode"`
}

func OfAlarmCode(prefix string, manu string, no int) *AlarmCode {
	var cs []string
	return &AlarmCode{prefix, manu, no, cs}
}

func (a *AlarmCode) Parse(addr int, val uint16) *AlarmCode {
	//no 最多支持36个，例如，最多支持36簇的编码
	pre := a.prefix + a.manu + utils.ToChe(int64(a.no))
	for i := 0; i < 16; i++ {
		b := ((val >> i) & 0x01) == 0x01

		if b {
			alarmcode := pre + utils.ToHex(int64(addr), 4) + utils.ToHex(int64(i), 1)
			a.Codes = append(a.Codes, alarmcode)
		}
	}
	return a
}

func (a *AlarmCode) Parse0x02(idx int, val byte, bitOff int) *AlarmCode {
	//no 最多支持36个，例如，最多支持36簇的编码
	pre := a.prefix + a.manu + utils.ToChe(int64(a.no))
	for i := 0; i < 8; i++ {
		b := ((val >> i) & 0x01) == 0x01

		if b {
			alarmcode := pre + utils.ToHex(int64(i+idx*8+bitOff), 5)
			a.Codes = append(a.Codes, alarmcode)
		}
	}
	return a
}

func NewAlarmE(acode string) AlarmE {
	return AlarmE{
		Acode: acode,
		Dt:    time.Now().UnixMilli(),
	}
}

func OfAlarmE(acode string) AlarmE {
	return AlarmE{
		Acode: acode,
		Dt:    time.Now().UnixMilli(),
	}
}
