package core

type PricePolicy struct {
	Tp    int     `json:"tp"` // 1:尖 2:峰 3:平 4:谷 5:深谷
	Tmb   int     `json:"tmb"`
	Tme   int     `json:"tme"`
	Price float64 `json:"price"`
}

// 充放电策略
type IoePolicy struct {
	Tmb  int `json:"tmb"` //开始时间 010800  01月08时00分
	Tme  int `json:"tme"` //结束时间 011800  01月18时00分
	Step int `json:"io"`  //工步 1充电，2放电，3休眠
}
