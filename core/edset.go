package core

import (
	"log/slog"
	"time"
)

// 整体EMS系统实时信息计算
type EDSet struct {
	Dt   int64     `json:"dt"`
	Attm time.Time `json:"attm"`
	//bms
	Bms_status  int     `json:"bms_status"`  //系统状态  0正常工作 0xFE 无效
	Bms_st_run  int     `json:"bms_st_run"`  //运行状态， 0 停止 1 启动
	Bms_soc     float64 `json:"bms_soc"`     //
	Bms_w       float64 `json:"bms_w"`       //功率
	Bms_q_i0    float64 `json:"bms_q_i0"`    //0点充电量
	Bms_q_o0    float64 `json:"bms_q_o0"`    //0点放电量
	Bms_q_i     float64 `json:"bms_q_i"`     //日充电量
	Bms_q_o     float64 `json:"bms_q_o"`     //
	Bms_q_i_sum float64 `json:"bms_q_i_sum"` //
	Bms_q_o_sum float64 `json:"bms_q_o_sum"` //
	//pcs
	Pcs_status  int     `json:"pcs_status"`  //Pcs状态  0正常工作 0xFE 无效
	Pcs_st_oo   int     `json:"pcs_st_oo"`   //并离网状态 0无效 1并网 2离网
	Pcs_st_run  int     `json:"pcs_st_run"`  //运行状态， 0 停止 1 启动 2启动中
	Pcs_st_ctrl int     `json:"pcs_st_ctrl"` //控制状态， 1 本地手动控制状态  2 本地自动控制状态 4 远程控制状态
	Pcs_st_err  int     `json:"pcs_st_err"`  //0无 1故障 2告警
	Pcs_acv     float64 `json:"pcs_acv"`     //交流侧电压
	Pcs_dcv     float64 `json:"pcs_dcv"`     //直流侧电压
	Pcs_acw     float64 `json:"pcs_acw"`     //pcs交流侧功率
	Pcs_dcw     float64 `json:"pcs_dcw"`     //pcs直流侧功率
	//电网
	Eg_status  int     `json:"eg_status"`  //电网状态  0断电 1放电2充电 0xFE 无效
	Eg_v       float64 `json:"eg_v"`       //电网电压
	Eg_w       float64 `json:"eg_w"`       //电网侧功率
	Eg_q_i0    float64 `json:"eg_q_i0"`    //0点购电量
	Eg_q_o0    float64 `json:"eg_q_o0"`    //0点上网电量
	Eg_q_i     float64 `json:"eg_q_i"`     //日购电量
	Eg_q_o     float64 `json:"eg_q_o"`     //日上网电量
	Eg_q_i_sum float64 `json:"eg_q_i_sum"` //总购电量
	Eg_q_o_sum float64 `json:"eg_q_o_sum"` //总上网电量
	//自耗电表
	Zh_status int     `json:"zh_status"` //自耗电表状态  0正常工作 0xFE 无效
	Zh_w      float64 `json:"zh_w"`      //系统损耗功率
	Zh_q0     float64 `json:"zh_q0"`     //0点系统损耗电量
	Zh_q      float64 `json:"zh_q"`      //日系统损耗电量
	Zh_q_sum  float64 `json:"zh_q_sum"`  //总系统损耗电量
	//负载
	Fz_status int     `json:"fz_status"` //负载电表状态  0正常工作 0xFE 无效
	Fz_w      float64 `json:"fz_w"`      //负载功率
	Fz_q0     float64 `json:"fz_q0"`     //0点负载电量
	Fz_q      float64 `json:"fz_q"`      //负载日用电量
	Fz_q_sum  float64 `json:"fz_q_sum"`  //负载总用电量
	//金额
	Eg_mny_i     float64 `json:"eg_mny_i"`     //日购电金额
	Eg_mny_i_sum float64 `json:"eg_mny_i_sum"` //总购电金额
	Eg_mny_o     float64 `json:"eg_mny_o"`     //日上网金额
	Eg_mny_o_sum float64 `json:"eg_mny_o_sum"` //总上网金额
	Fz_mny       float64 `json:"fz_mny"`       //负载日用电金额
	Fz_mny_sum   float64 `json:"fz_mny_sum"`   //负载总用电金额
	Mny          float64 `json:"mny"`          //日收益
	Mny_sum      float64 `json:"mny_sum"`      //总收益

}

func (d *EDSet) NewDay() {
	//新的一天，重新计算日数据
	//0点数据
	d.Bms_q_o0 = d.Bms_q_o_sum
	d.Bms_q_i0 = d.Bms_q_i_sum
	d.Eg_q_i0 = d.Eg_q_i_sum
	d.Eg_q_o0 = d.Eg_q_o_sum
	d.Zh_q0 = d.Zh_q_sum
	d.Fz_q0 = d.Fz_q_sum
	//日数据
	d.Bms_q_i = 0
	d.Bms_q_o = 0
	d.Eg_q_i = 0
	d.Eg_q_o = 0
	d.Zh_q = 0
	d.Fz_q = 0
	//日金额
	d.Fz_mny = 0
	d.Mny = 0
	slog.Debug("core.ems.go", "EMS执行跨天操作==", d)
}

// 柴发备电EMS
type EmsCFBD struct {
	EDSet
	Bding      bool    //正在备电
	Bd_dtb     int64   //备电开始时间（秒）
	Bd_tl      int64   //累计备电时长(秒)
	Bd_tms     int     //累计备电次数
	Fd_status  int     //发电电表状态  0正常工作 0xFE 无效
	Fd_w       float64 //发电功率
	Fd_q0      float64 //0点发电量
	Fd_q       float64 //日发电量
	Fd_q_sum   float64 //总发电量
	Sts_status int     //STS状态  0停机 1并网 2离网 0xFE 无效
	Sts_isOpen bool    `default:"true"` // sts 是否并网，true并网，false离网
	Sts_io_tms int     //sts切换次数
	Sts_w_eg   float64 //sts 电网侧功率
	Sts_w_fz   float64 //sts 负载功率
	Sw_status  int     //ATS开关状态  0正常工作 0xFE 无效
	Sw_01      int     //Ats 电网侧开关状态  0断开 1接通 0xFE 无效
	Sw_02      int     //Ats 柴发侧开关状态  0断开 1接通 0xFE 无效
}

type CPctrl struct {
	Dt         int64
	Bms_soc    float64 //SOC
	Bms_iw_max float64 //
	Bms_ow_max float64 //
	Pcs_acw1   float64 //
	Amt_v_eg   float64 //
	Amt_w_eg   float64 //
	Sts_w_eg   float64 //
	Sts_w_fz   float64 //
	Cmd_pcs_w  float64 //设定功率值
}

func (d *EmsCFBD) NewDay() {
	d.EDSet.NewDay()
	//新的一天，重新计算日数据
	d.Fd_q = 0
	d.Fd_q0 = d.Fd_q_sum

	slog.Debug("core.ems.go", "备电执行跨天操作==", d)
}
