package core

import "gitee.com/sgems/sg.core/utils"

//昆宇自研EMS产生的告警码编制，主要为寄存器地址及告警位编制
//[前缀][设备厂家][设备序号][寄存器地址][告警位]
// E KY 0 [寄存器地址][告警位]

/*
	通讯异常告警：寄存器地址0x0001
		bit0 BMS
		bit1 PCS
		bit2 STS
		bit3 电表
		bit4 柴发
		bit5 光伏
*/

/*
	数据异常告警：寄存器地址0x0002
		bit0 BMS
		bit1 PCS
		bit2 STS
		bit3 电表
		bit4 柴发
		bit5 光伏
*/

const (
	KYEMS_BIT_BMS  = 0
	KYEMS_BIT_PCS  = 1
	KYEMS_BIT_STS  = 2
	KYEMS_BIT_AMT  = 3
	KYEMS_BIT_柴发   = 4
	KYEMS_BIT_PV   = 5
	KYEMS_BIT_GPIO = 6

	KYEMS_ADDR_通讯异常 = 0x0001
	KYEMS_ADDR_数据无效 = 0x0002
	KYEMS_ADDR_状态异常 = 0x0003
)

func OfAlarmCode_KYEMS(no int64, addr uint16, bit uint8) string {
	return "EKY" + utils.ToChe(no) + utils.ToHex(int64(addr), 4) + utils.ToHex(int64(bit), 1)
}

func OfAlarm_KYEMS(no int64, addr uint16, bit uint8) *AlarmE {
	acode := "EKY" + utils.ToChe(no) + utils.ToHex(int64(addr), 4) + utils.ToHex(int64(bit), 1)
	ac := OfAlarmE(acode)
	return &ac
}
