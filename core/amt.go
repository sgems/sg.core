package core

type CAmtE struct {
	Dt    int64
	No    int
	P_i   float64 //功率 入
	P_o   float64 //功率 出
	Q_o   float64 //日出电电量
	Q_i   float64 //日入电电量
	Q0_o  float64
	Q0_i  float64
	Q1_o  float64
	Q1_i  float64
	Price float64
}

type AmtE struct {
	Attm     string  `json:"attm"`
	Dt       int64   `json:"dt"`
	No       int     `json:"no"`
	W        float64 `json:"w"` //功率
	V        float64 `json:"v"`
	Hz       float64 `json:"hz"`       //频率 Hz
	Eq_i_sum float64 `json:"eq_i_sum"` //总购电量
	Eq_o_sum float64 `json:"eq_o_sum"` //总上网电量
}
