package core

type (
	SgGpio struct {
		Di01 int `json:"di01"`
		Di02 int `json:"di02"`
		Di03 int `json:"di03"`
		Di04 int `json:"di04"`
		Di05 int `json:"di05"`
		Di06 int `json:"di06"`
		Di07 int `json:"di07"`
		Di08 int `json:"di08"`
	}
)

const (
	GPIO_LINK = 0
	GPIO_DISL = 1
)

func OfSgGpio() *SgGpio {
	return &SgGpio{
		Di01: GPIO_DISL,
		Di02: GPIO_DISL,
		Di03: GPIO_DISL,
		Di04: GPIO_DISL,
		Di05: GPIO_DISL,
		Di06: GPIO_DISL,
		Di07: GPIO_DISL,
		Di08: GPIO_DISL,
	}
}
