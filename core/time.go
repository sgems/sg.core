package core

import "time"

type MbTime struct {
	Yy uint16
	Mn uint16
	Dd uint16
	Hh uint16
	Mi uint16
	Ss uint16
}

func NowMbTime() *MbTime {
	tm := time.Now()
	return &MbTime{
		Yy: uint16(tm.Year()),
		Mn: uint16(tm.Month()),
		Dd: uint16(tm.Day()),
		Hh: uint16(tm.Hour()),
		Mi: uint16(tm.Minute()),
		Ss: uint16(tm.Second())}
}

func (t *MbTime) ToBytes() []byte {
	return []byte{byte(t.Yy), byte(t.Mn), byte(t.Dd), byte(t.Hh), byte(t.Mi), byte(t.Ss)}
}
