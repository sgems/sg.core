package core

func ToFloat(v any, f float64) float64 {
	switch val := v.(type) {
	case uint16:
		return f * float64(val)
	case int16:
		return f * float64(val)
	case uint32:
		return f * float64(val)
	case int32:
		return f * float64(val)
	default:
		return 0
	}
}

func ToFloat10(v any) float64 {
	return ToFloat(v, 0.1)
}

func ToFloat100(v any) float64 {
	return ToFloat(v, 0.01)

}

func ToFloat1000(v any) float64 {
	return ToFloat(v, 0.001)

}
