package core

//实时计算结果
type CPcsE struct {
	Dt     int64   `json:"dt"`
	No     int     `json:"no"`
	Acw1   float64 `json:"acw1"`   //PCS有功功率
	Acw2   float64 `json:"acw2"`   //PCS无功功率
	Acw3   float64 `json:"acw3"`   //PCS视在功率
	Dc_w   float64 `json:"dc_w"`   //直流功率
	T_mode float64 `json:"t_mode"` //模块温度
	T_env  float64 `json:"t_env"`  //环境温度
}

// 状态数据
// type PcsStE struct {
// 	Attm        string `json:"attm"`
// 	Dt          int64  `json:"dt"`
// 	No          int    `json:"no"`
// 	Status      int    `json:"status"`      //0无效 1并网 2离网
// 	Status_run  int    `json:"status_run"`  //运行状态， 0 停止 1 启动 2启动中
// 	Status_ctrl int    `json:"status_ctrl"` //控制状态， 1 本地手动控制状态  2 本地自动控制状态 4 远程控制状态
// 	Status_err  int    `json:"status_err"`  //0无 1故障 2告警
// }

// PCS交流数据
type PcsE struct {
	Attm string `json:"attm"`
	Dt   int64  `json:"dt"`
	No   int    `json:"no"`
	//状态数据--------------------
	St_oo   int `json:"st_oo"`   //并离网状态，0无效 1并网 2离网
	St_run  int `json:"st_run"`  //运行状态， 0 停止 1 启动 2启动中
	St_ctrl int `json:"st_ctrl"` //控制状态， 1 本地手动控制状态  2 本地自动控制状态 4 远程控制状态
	St_err  int `json:"st_err"`  //0无 1故障 2告警
	//交流数据--------------------
	Lv_ac_a float64 `json:"v_ac_a"` //ab交流线电压
	Lv_ac_b float64 `json:"v_ac_b"` //bc交流线电压
	Lv_ac_c float64 `json:"v_ac_c"` //ca交流线电压
	C_ac_a  float64 `json:"c_ac_a"` //A相交流电流
	C_ac_b  float64 `json:"c_ac_b"` //b相交流电流
	C_ac_c  float64 `json:"c_ac_c"` //c相交流电流
	W1      float64 `json:"w1"`     //PCS有功功率
	W2      float64 `json:"w2"`     //PCS无功功率
	W3      float64 `json:"w3"`     //PCS视在功率
	Wf      float64 `json:"wf"`     //功率因数
	Hz      float64 `json:"hz"`     //交流频率
	//直流数据--------------------
	W_dc float64 `json:"w_dc"` //总直流功率
	V_dc float64 `json:"v_dc"` //总直流电压
	C_dc float64 `json:"c_dc"` //总直流电流
	//温度数据--------------------
	T_env  float64 `json:"t_env"`  //环境温度
	T_mode float64 `json:"t_mode"` //模块温度
	//设置数据--------------------
	W_set float64 `json:"w_set"` //有功功率设置值
}

// // PCS直流数据
// type PcsDcE struct {
// 	Attm      string  `json:"attm"`
// 	Dt        int64   `json:"dt"`
// 	No        int     `json:"no"`
// 	W_dc_sub1 float64 `json:"w_dc_sub1"` //支路1：直流功率 53250
// 	V_dc_sub1 float64 `json:"v_dc_sub1"` //支路1：直流电压 53251
// 	C_dc_sub1 float64 `json:"c_dc_sub1"` //支路1：直流电流 53252
// 	W_dc_sub2 float64 `json:"w_dc_sub2"` //支路2：直流功率 53270
// 	V_dc_sub2 float64 `json:"v_dc_sub2"` //支路2：直流电压 53271
// 	C_dc_sub2 float64 `json:"c_dc_sub2"` //支路2：直流电流 53272
// 	W_dc_sub3 float64 `json:"w_dc_sub3"` //支路3：直流功率 53290
// 	V_dc_sub3 float64 `json:"v_dc_sub3"` //支路3：直流电压 53291
// 	C_dc_sub3 float64 `json:"c_dc_sub3"` //支路3：直流电流 53292
// 	W_dc_sub4 float64 `json:"w_dc_sub4"` //支路4：直流功率 53310
// 	V_dc_sub4 float64 `json:"v_dc_sub4"` //支路4：直流电压 53311
// 	C_dc_sub4 float64 `json:"c_dc_sub4"` //支路4：直流电流 53312
// 	W_dc_sub5 float64 `json:"w_dc_sub5"` //支路5：直流功率 53330
// 	V_dc_sub5 float64 `json:"v_dc_sub5"` //支路5：直流电压 53331
// 	C_dc_sub5 float64 `json:"c_dc_sub5"` //支路5：直流电流 53332
// 	W         float64 `json:"w"`         //总功率
// 	V         float64 `json:"v"`         //总电压
// 	C         float64 `json:"c"`         //总电流
// }

// // PCS设置数据
// type PcsSetE struct {
// 	Attm  string  `json:"attm"`
// 	Dt    int64   `json:"dt"`
// 	No    int     `json:"no"`
// 	W_set float64 `json:"w_set"` //设定的功率值
// }

// // pcs 各支路有功功率设置值
// type PcsSubSetE struct {
// 	Attm   string  `json:"attm"`
// 	Dt     int64   `json:"dt"`
// 	No     int     `json:"no"`
// 	W_set1 float64 //支路1：有功功率设置 55600
// 	W_set2 float64 //支路2：有功功率设置 55601
// 	W_set3 float64 //支路3：有功功率设置 55602
// 	W_set4 float64 //支路4：有功功率设置 55603
// 	W_set5 float64 //支路5：有功功率设置 55604
// 	W_set6 float64 //支路6：有功功率设置 55605
// 	W_set7 float64 //支路7：有功功率设置 55606
// 	W_set8 float64 //支路8：有功功率设置 55607
// }
