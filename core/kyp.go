package core

import (
	"sync"
	"time"

	"gitee.com/sgems/sg.core/utils"
)

// 昆宇网络通讯协议，可用json 组织传输协议

type KyTrans struct {
	Code   string       `json:"code"` //编码
	Dt     int64        `json:"dt"`   //时间戳
	Attm   string       `json:"attm"` //时间
	Prot   string       `json:"prot"` //协议 BMS|PCS|STS|MPPT|  协能|盛宏|盛宏|无：02030300
	Bms    []*BmsE      `json:"bms"`
	Pcs    []*PcsE      `json:"pcs"`
	Sts    []*StsE      `json:"sts"`
	Eg     []*AmtE      `json:"eg"`
	Fz     []*AmtE      `json:"fz"`
	Edset  []*EDSet     `json:"edset"`
	Alarms []*AlarmE    `json:"alarms"`
	mu     sync.RWMutex `json:"-"` //读写锁
}

func OfKyTrans(code string, dt int64, prot string) *KyTrans {
	return &KyTrans{
		Code: code,
		Dt:   dt,
		Attm: utils.Date2YYYY_MM_dd_HH_mm_ss(time.UnixMilli(dt)),
		Prot: prot,
	}
}

func (k *KyTrans) PushPcs(p *PcsE) *KyTrans {
	if k.Pcs == nil {
		k.Pcs = make([]*PcsE, 0)
	}
	k.Pcs = append(k.Pcs, p)
	return k
}

func (k *KyTrans) PushSts(s *StsE) *KyTrans {
	if k.Sts == nil {
		k.Sts = make([]*StsE, 0)
	}
	k.Sts = append(k.Sts, s)
	return k
}

func (k *KyTrans) PushEg(e *AmtE) *KyTrans {
	if k.Eg == nil {
		k.Eg = make([]*AmtE, 0)
	}
	k.Eg = append(k.Eg, e)
	return k
}

func (k *KyTrans) PushFz(f *AmtE) *KyTrans {
	if k.Fz == nil {
		k.Fz = make([]*AmtE, 0)
	}
	k.Fz = append(k.Fz, f)
	return k
}

func (k *KyTrans) PushEdset(e *EDSet) *KyTrans {
	if k.Edset == nil {
		k.Edset = make([]*EDSet, 0)
	}
	k.Edset = append(k.Edset, e)
	return k
}

func (k *KyTrans) PushAlarm(a *AlarmE) *KyTrans {
	if k.Alarms == nil {
		k.Alarms = make([]*AlarmE, 0)
	}
	k.Alarms = append(k.Alarms, a)
	return k
}

func (k *KyTrans) PushBms(b *BmsE) *KyTrans {
	if k.Bms == nil {
		k.Bms = make([]*BmsE, 0)
	}
	k.Bms = append(k.Bms, b)
	return k
}

func (k *KyTrans) Init(code string, dt int64, prot string) {
	k.Code = code
	k.Dt = dt
	k.Attm = utils.Date2YYYY_MM_dd_HH_mm_ss(time.UnixMilli(dt))
	k.Prot = prot
	k.Pcs = nil
	k.Sts = nil
	k.Eg = nil
	k.Fz = nil
	k.Edset = nil
	k.Alarms = nil
	k.Bms = nil
}

func (k *KyTrans) ToSafeJson(code string, dt int64, prot string) []byte {
	k.mu.Lock()
	defer k.mu.Unlock()
	bs := utils.ToJsonBytes(k)
	k.Init(code, dt, prot)
	return bs
}
