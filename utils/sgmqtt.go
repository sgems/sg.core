package utils

import (
	"log/slog"
	"sync/atomic"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type SgMqttMessageHandler func(*mqtt.Message)

type SgMqtt struct {
	client    mqtt.Client
	eChan     chan *mqtt.Message
	url       string
	clientId  string
	uname     string
	pwd       string
	subTopics []string
	connected atomic.Bool
	onMessage SgMqttMessageHandler
}

func OfSgMqtt(_url, cid, _uname, _pwd string, subTopics []string, msgHandler SgMqttMessageHandler) *SgMqtt {
	mqtt := &SgMqtt{
		url:       _url,
		clientId:  cid + "_" + NowDateTimeString(),
		uname:     _uname,
		pwd:       _pwd,
		eChan:     make(chan *mqtt.Message, 1000),
		subTopics: subTopics,

		onMessage: msgHandler,
	}
	go func() {
		for {
			if !mqtt.connected.Load() {
				mqtt.connect()
			}
			slog.Debug("Mqtt", " Connect status", mqtt.connected.Load())
			time.Sleep(time.Second * 10)
		}
	}()
	go mqtt.listionReceiveMsg()
	return mqtt
}

func (t *SgMqtt) connect() {

	opts := mqtt.NewClientOptions()
	opts.AddBroker(t.url)
	opts.SetClientID(t.clientId)
	opts.SetUsername(t.uname)
	opts.SetPassword(t.pwd)
	opts.SetAutoReconnect(false)
	opts.SetConnectRetry(false)
	// opts.SetConnectRetryInterval(10 * time.Second)
	opts.SetDefaultPublishHandler(t.onmessagePub)
	opts.OnConnect = t.onConnect
	opts.OnConnectionLost = t.onconnectLost
	t.client = mqtt.NewClient(opts)
	t.alwaysCheckConnect()
}

func (t *SgMqtt) alwaysCheckConnect() {

	if token := t.client.Connect(); token.Wait() && token.Error() != nil {
		t.connected.Store(false)
		slog.Error("Ems ", "MQTT 连接失败！", token.Error(), "url", t.url)
		return
	}
	slog.Debug("Ems ", "MQTT 连接成功！", "url", t.url)
	t.connected.Store(true)

}

func (t *SgMqtt) SendJson(topic string, json []byte) bool {
	// fmt.Println("SendJson client================", t.client)
	if t.client == nil || !t.client.IsConnected() {
		return false
	}
	// slog.Debug("web开始发送指令", "执行topic", topic, "url", t.url, "json", string(json))
	return t.client.Publish(topic, 0, false, json).Wait()
}

func (t *SgMqtt) IsConnected() bool {
	if t.client == nil || !t.client.IsConnected() {
		return false
	}
	return true
}

func (t *SgMqtt) sub() {
	if t.subTopics == nil {
		return
	}
	for _, topic := range t.subTopics {
		t.client.Subscribe(topic, 1, nil).Wait()
		slog.Info("订阅", "topic", topic)
	}
}

func (t *SgMqtt) onmessagePub(client mqtt.Client, msg mqtt.Message) {
	t.eChan <- &msg
}

func (t *SgMqtt) onConnect(client mqtt.Client) {
	t.connected.Store(true)
	t.client = client
	t.sub()
	// slog.Info("sssEms Mqtt Connected!", "url", t.url, "client", t.client)
}

func (t *SgMqtt) onconnectLost(client mqtt.Client, err error) {
	t.connected.Store(false)
	slog.Error("Ems Mqtt Connect lost", "err", err, "url", t.url)
}

func (t *SgMqtt) listionReceiveMsg() {
	for msg := range t.eChan {
		t.onMessage(msg)
	}

}

func (t *SgMqtt) Disconnect() {
	t.client.Disconnect(1)
}
