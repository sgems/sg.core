package utils

func Max(a float64, b float64) float64 {
	if a > b {
		return a
	}
	return b
}

func Max3(a float64, b float64, c float64) float64 {
	return Max(Max(a, b), c)
}

func Min(a float64, b float64) float64 {
	if a > b {
		return b
	}
	return a
}

func Min3(a float64, b float64, c float64) float64 {
	return Min(Min(a, b), c)
}

// 约等于，tol  容差
func Aequal(a float64, b float64, tol float64) bool {
	return Max(a, b)-Min(a, b) < tol
}

// 三元运算
func If3[T any](b bool, v1, v2 T) T {
	if b {
		return v1
	} else {
		return v2
	}
}

// 按位赋值
func BitSet(src, bit, val int) int {
	mask := 1 << bit
	n := If3[int](val > 0, 1, 0)
	target := src | mask
	if n == 0 {
		target = src &^ mask
	}
	return target
}
