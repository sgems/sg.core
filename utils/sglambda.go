package utils

type FilterFunc[T any] func(T) bool
type MapFunc[T any, R any] func(T) R
type ParamFunc1[T any] func(T)

type SgLambda[T any] struct {
	source []T
}

func OfSgtream[T any](source []T) *SgLambda[T] {
	return &SgLambda[T]{source: source}
}
func (sg *SgLambda[T]) Like(f func(f T) T) *SgLambda[T] {
	var rst []T
	for _, v := range sg.source {
		r := f(v)
		rst = append(rst, r)
	}
	sg.source = rst
	return sg
}

func (sg *SgLambda[T]) Filter(f FilterFunc[T]) *SgLambda[T] {
	var rst []T
	for _, v := range sg.source {
		if r := f(v); r {
			rst = append(rst, v)
		}
	}
	sg.source = rst
	return sg
}

func (sg *SgLambda[T]) Concat(objs ...T) *SgLambda[T] {
	sg.source = append(sg.source, objs...)
	return sg
}

func (sg *SgLambda[T]) Map(f MapFunc[T, T]) *SgLambda[T] {
	var rst []T
	for _, v := range sg.source {
		rst = append(rst, f(v))
	}
	sg.source = rst
	return sg
}
func (sg *SgLambda[T]) Each(f func(f T)) *SgLambda[T] {
	for _, v := range sg.source {
		f(v)
	}
	return sg
}
func (sg *SgLambda[T]) Get() []T {
	return sg.source
}

func Map[T any, R any](a []T, m MapFunc[T, R]) []R {
	var n []R
	for _, e := range a {
		v := m(e)
		n = append(n, v)
	}
	return n
}

func Filter[T any](source []T, f FilterFunc[T]) []T {
	var rst []T
	for _, v := range source {
		if r := f(v); r {
			rst = append(rst, v)
		}
	}
	return rst
}
