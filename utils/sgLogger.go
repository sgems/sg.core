package utils

import (
	"fmt"
	"log/slog"
	"os"

	"gopkg.in/natefinch/lumberjack.v2"
)

// 是否保存到文件中，打印日志级别，日志保留天数
func InitLogger(fileName string, inFile bool, lv string, day int) {
	lev := slog.LevelInfo
	if lv == "debug" {
		lev = slog.LevelDebug
	}
	if lv == "warn" {
		lev = slog.LevelWarn
	}
	if lv == "error" {
		lev = slog.LevelError
	}

	op := &slog.HandlerOptions{
		AddSource: false,
		Level:     lev,
	}

	if inFile {
		foo := fileName
		if fileName == "" {
			foo = ConcatPath(AppPath(), "foo.log")
		}

		fmt.Println("日志路径=========", foo)
		r := &lumberjack.Logger{
			Filename:   foo,
			LocalTime:  true,
			MaxSize:    100,  //每个日志文件，最大100M
			MaxBackups: 10,   //最多保留10个日志文件
			MaxAge:     day,  //最多保留 day 天
			Compress:   true, //归档后压缩
		}
		logger := slog.New(slog.NewTextHandler(r, op))
		slog.SetDefault(logger)
	} else {
		logger := slog.New(slog.NewTextHandler(os.Stdout, op))
		slog.SetDefault(logger)
	}

}
