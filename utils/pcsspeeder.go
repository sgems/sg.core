package utils

import (
	"strconv"
	"time"

	"github.com/patrickmn/go-cache"
)

//pcs充电功率变速器，将充电功率由一个值匀速变到另一个值
//充电为正，放电为负
//充电功率增大时匀速变化
//其他情况下，直接调整功率

type PcsSpeeder struct {
	cache    *cache.Cache
	subCount int
	//时间间隔
	Interval int
	//变化速度
	Speed  int
	Mode   int //变化模式 0正负都不变；1正变；2负变；3正负都变
	wCh    chan *Tup2[int, int]
	cbfunc ParamFunc1[*Tup2[int, int]]
}

const (
	KEY_CUR = "cur"
	KEY_TGT = "tgt"
)

func NewPcsSpeeder(speed, subCount, interval, keepSec int) *PcsSpeeder {
	return &PcsSpeeder{
		cache:    cache.New(time.Duration(keepSec)*time.Second, 5*time.Second),
		Interval: interval,
		Speed:    speed,
		subCount: subCount,
		wCh:      make(chan *Tup2[int, int], 5),
	}
}

func (wsp *PcsSpeeder) Start() *PcsSpeeder {
	go wsp.work(1)
	go wsp.listenSpeed()
	return wsp
}

// 设置 pcs当前采集到的设定功率
func (wsp *PcsSpeeder) SetCurVal(no int, val int) *PcsSpeeder {
	k := wsp.newKey(KEY_CUR, no)
	wsp.cache.SetDefault(k, val)
	return wsp
}

// 设置 当前需要向pcs发送的 设定功率
func (wsp *PcsSpeeder) SetTargetVal(no int, val int) *PcsSpeeder {
	k := wsp.newKey(KEY_TGT, no)
	wsp.cache.SetDefault(k, val)
	return wsp
}

func (wsp *PcsSpeeder) newKey(pre string, no int) string {
	return pre + ":" + strconv.Itoa(no)
}

func (wsp *PcsSpeeder) peekVal(no int) (cur, tgt int, ok bool) {
	cur = 0
	tgt = 0
	ok = false
	v_cur, ok_cur := wsp.cache.Get(wsp.newKey(KEY_CUR, no))
	v_tgt, ok_tgt := wsp.cache.Get(wsp.newKey(KEY_TGT, no))
	if ok_cur && ok_tgt {
		cur = v_cur.(int)
		tgt = v_tgt.(int)
		ok = true
	}
	return cur, tgt, ok
}

func (wsp *PcsSpeeder) work(bNo int) {
	interval := time.Duration(wsp.Interval) * time.Millisecond
	for {
		for i := 0; i < wsp.subCount; i++ {
			wsp.subWork(bNo + i)
		}
		time.Sleep(interval)
	}
}
func (wsp *PcsSpeeder) subWork(no int) {

	v_cur, v_tgt, ok := wsp.peekVal(no)
	if !ok {
		return
	}

	if v_cur == v_tgt {
		return
	}
	//如果目标值大于0，则充电；小于0，则放电
	if v_tgt > 0 {
		v_b := If3[int](v_cur < 0, 0-wsp.Speed, v_cur)
		d := v_tgt - v_b
		if d > 0 {
			v := v_b + wsp.Speed
			if v > v_tgt {
				v = v_tgt
			}
			//发送功率
			wsp.wCh <- &Tup2[int, int]{A: no, B: v}
			return
		}
	}
	//发送功率
	wsp.wCh <- &Tup2[int, int]{A: no, B: v_tgt}
}

func (wsp *PcsSpeeder) SetListenFunc(cbfunc ParamFunc1[*Tup2[int, int]]) *PcsSpeeder {
	wsp.cbfunc = cbfunc
	return wsp
}

func (wsp *PcsSpeeder) listenSpeed() {
	for {
		v := <-wsp.wCh
		//发送功率
		wsp.cbfunc(v)
	}
}
