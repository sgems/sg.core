package utils

type Tup2[A, B any] struct {
	A A
	B B
}
type Tup3[A, B, C any] struct {
	A A
	B B
	C C
}
type Tup4[A, B, C, D any] struct {
	A A
	B B
	C C
	D D
}
type Tup5[A, B, C, D, E any] struct {
	A A
	B B
	C C
	D D
	E E
}
type Tup6[A, B, C, D, E, F any] struct {
	A A
	B B
	C C
	D D
	E E
	F F
}
type Tup7[A, B, C, D, E, F, G any] struct {
	A A
	B B
	C C
	D D
	E E
	F F
	G G
}
type Tup8[A, B, C, D, E, F, G, H any] struct {
	A A
	B B
	C C
	D D
	E E
	F F
	G G
	H H
}
type Tup9[A, B, C, D, E, F, G, H, I any] struct {
	A A
	B B
	C C
	D D
	E E
	F F
	G G
	H H
	I I
}
