package utils

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"os"
)

func Sggzip(path string, dest string) {
	fw, _ := os.Create(dest)
	defer fw.Close()
	gw := gzip.NewWriter(fw) //gzipWriter:需要操作的句柄
	defer gw.Close()

	tw := tar.NewWriter(gw)
	defer tw.Close()

	file, _ := os.Open(path)
	defer file.Close()
	err := compress(file, "sg", tw)
	if err != nil {
		fmt.Println("open file error", err)
	}
	log.Println("success")
	fmt.Println("*******************zzzzzzzzzzzzzzzzzzzzzzz")
}

func Mygzip(path string, dest string) {
	fw, _ := os.Open(dest)
	defer fw.Close()
	// 实例化心得gzip.Writer
	gw := gzip.NewWriter(fw)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()
	file, _ := os.Open(path)
	defer file.Close()
	err := compress(file, "sg", tw)
	if err != nil {
		fmt.Println("dddd file error", err)
	}
	fmt.Println("*******************ddddddddddddddddd")
}

func SgZip(path string, dest string) error {
	d, _ := os.Create(dest)
	defer d.Close()
	zw := tar.NewWriter(d)
	defer zw.Close()
	//遍历path下所有文件
	file, _ := os.Open(path)
	defer file.Close()
	return compress(file, "sg", zw)
}

func compress(file *os.File, prefix string, tw *tar.Writer) error {
	info, err := file.Stat()
	if err != nil {
		return err
	}
	if info.IsDir() {
		prefix = prefix + "/" + info.Name()
		fileInfos, err := file.Readdir(-1)
		if err != nil {
			return err
		}
		for _, fi := range fileInfos {
			f, err := os.Open(file.Name() + "/" + fi.Name())
			if err != nil {
				return err
			}
			err = compress(f, "", tw)
			if err != nil {
				return err
			}
		}
	} else {
		header, err := tar.FileInfoHeader(info, "")
		header.Name = prefix + "/" + header.Name
		if err != nil {
			return err
		}
		err = tw.WriteHeader(header)
		if err != nil {
			return err
		}
		_, err = io.Copy(tw, file)
		file.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
