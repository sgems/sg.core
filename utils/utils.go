package utils

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log/slog"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var (
	HEX_CHARS = []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}
	CHE_CHARS = []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z"}
)

func Tmstring2int(tm string) int {
	str := strings.ReplaceAll(tm, ":", "")
	n, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return n
}

func ToCsvString(input any) ([]string, []string) {
	getType := reflect.TypeOf(input)
	getValue := reflect.ValueOf(input)
	if getType.Kind() == reflect.Ptr {
		getType = getType.Elem()
		getValue = getValue.Elem()
	}
	if getType.Kind() != reflect.Struct {
		slog.Error("ToCsvString", "err", "Check type error not Struct")
		return nil, nil
	}
	titles := []string{}
	vals := []string{}
	for i := 0; i < getType.NumField(); i++ {
		field := getType.Field(i)
		titles = append(titles, field.Name)

		value := getValue.Field(i).Interface()
		vals = append(vals, fmt.Sprintf("%v", value))
	}
	return titles, vals
}

func ToCsvNAppendAttm(name string, dt int64, input any) Tup3[string, []string, []string] {
	tits, vals := ToCsvString(input)
	tits = append(tits, "attm")
	tm := time.UnixMilli(dt)
	vals = append(vals, Date2YYYY_MM_dd_HH_mm_ss_000(tm))

	return Tup3[string, []string, []string]{A: name, B: tits, C: vals}
}

func ToCsvTitle(input any) []string {
	getType := reflect.TypeOf(input)
	titles := []string{}
	for i := 0; i < getType.NumField(); i++ {
		field := getType.Field(i)
		titles = append(titles, field.Name)
	}
	return titles
}

func ToCsvValue(input any) []string {
	getValue := reflect.ValueOf(input)
	vals := []string{}
	for i := 0; i < getValue.NumField(); i++ {
		value := getValue.Field(i).Interface()
		vals = append(vals, fmt.Sprintf("%v", value))
	}
	return vals
}

func ToCsvStrings[T any](inputs []T) ([]string, [][]string) {
	input := inputs[0]

	title := ToCsvTitle(input)
	vals := [][]string{}
	for _, v := range inputs {
		value := ToCsvValue(v)
		vals = append(vals, value)

	}
	return title, vals
}

func ToJsonBytes(v any) []byte {
	jn, _ := json.Marshal(v)
	return jn
}
func ToJsonString(v any) string {
	jn, _ := json.Marshal(v)
	return string(jn)
}

func FromJsonBytes[T any](s []byte) (T, bool) {
	var e T
	err := json.Unmarshal(s, &e)
	if err != nil {
		slog.Error("json解析失败", "err", err, "数据源", string(s))
		return e, false
	}
	return e, true
}
func ToChe(val int64) string {
	if val > 35 {
		return "z"
	}
	return CHE_CHARS[val]
}

func ToHex(val int64, len int) string {
	rst := ""
	for i := len - 1; i >= 0; i-- {
		rst += HEX_CHARS[(val>>(4*i))&0x0F]
	}
	return rst
}

func LastCut(s, sep string) (before, after string, found bool) {
	if i := strings.LastIndex(s, sep); i >= 0 {
		return s[:i], s[i+len(sep):], true
	}
	return s, "", false
}

func ConcatPath(paths ...any) string {
	all := JoinString("/", paths...)
	all = strings.ReplaceAll(all, "\\", "/")
	all = strings.ReplaceAll(all, "//", "/")
	return strings.ReplaceAll(all, "//", "/")
}
func JoinString(sep string, src ...any) string {
	var sb strings.Builder
	for i, v := range src {
		sb.WriteString(fmt.Sprintf("%v", v))
		if i < len(src)-1 {
			sb.WriteString(sep)
		}
	}
	return sb.String()
}
func ToStrings(src ...any) []string {
	var ss []string
	for _, v := range src {
		ss = append(ss, fmt.Sprintf("%v", v))
	}
	return ss
}
func ToAscString(m any) string {
	buf := &bytes.Buffer{}
	err := binary.Write(buf, binary.BigEndian, m)
	if err != nil {
		panic(err)
	}
	encodedStr := hex.EncodeToString(buf.Bytes())
	return strings.ToUpper(encodedStr)
}

func ToHexString(m any) string {
	buf := &bytes.Buffer{}
	err := binary.Write(buf, binary.BigEndian, m)
	if err != nil {
		panic(err)
	}
	encodedStr := fmt.Sprintf("%x", buf.Bytes())
	return strings.ToUpper(encodedStr)
}

func NowString() string {
	d := time.Now()
	str := d.Format("2006-01-02 15:04:05")
	return str
}

func NowTimeString() string {
	d := time.Now()
	str := d.Format("150405")
	return str
}

func NowDateTimeString() string {
	d := time.Now()
	str := d.Format("060102150405")
	return str
}

func Date2YYYY_MM_dd_HH_mm_ss(attm time.Time) string {
	str := attm.Local().Format("2006-01-02 15:04:05")
	return str
}

func Date2YYYY_MM_dd_HH_mm_ss_000(attm time.Time) string {
	str := attm.Local().Format("2006-01-02 15:04:05.000")
	return str
}

func Date2YYYY_MM_dd(attm time.Time) string {
	str := attm.Local().Format("2006-01-02")
	return str
}

func AddHour(attm time.Time, hour int) time.Time {
	return attm.Add(time.Duration(hour) * time.Hour)
}

func AppPath() string {
	f, _ := exec.LookPath(os.Args[0])
	return filepath.Dir(f)
}

func ValidCrc16(bs []byte) bool {
	n := len(bs)
	if n < 3 {
		return false
	}

	res := 0xFFFF
	for _, data := range bs[:n-2] {
		res = res ^ int(data)
		for i := 0; i < 8; i++ {
			if (res & 0x0001) == 1 {
				res = (res >> 1) ^ 0xA001
			} else {
				res = res >> 1
			}
		}
	}
	b1 := bs[n-2]
	b2 := bs[n-1]
	if byte(res&0xFF) == b1 && byte((res>>8)&0xFF) == b2 {
		return true
	}
	return false
}

func Avg_float64(fs ...float64) float64 {
	f := 0.0
	for i := 0; i < len(fs); i++ {
		f += fs[i]
	}
	return f / float64(len(fs))
}

func TmHexString() string {
	tm := time.Now().UnixMilli()
	return ToHexString(tm)
}
