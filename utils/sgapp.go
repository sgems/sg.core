package utils

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"log/slog"
)

type SgFunc func()

// 优雅退出go守护进程
func Start_sg_app(startFunc SgFunc, exitFunc SgFunc) {
	//创建监听退出chan
	c := make(chan os.Signal)
	//监听指定信号 ctrl+c kill
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		for s := range c {
			switch s {
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
				slog.Info("收到退出信号，开始退出系统...", "信号", s)

				exitFunc()
				slog.Info("系统已退出！！！")
				os.Exit(0)

			default:
				fmt.Println("other", "信号", s)
			}
		}
	}()

	slog.Info("开始启动系统...")
	startFunc()
	slog.Info("系统启动完毕！")
	for {
		time.Sleep(time.Second)
	}

}
