package utils

import (
	"strconv"
	"time"

	"github.com/patrickmn/go-cache"
)

// 数据聚合，以时间为聚合条件
type SgTimeAggr[T any] struct {
	c        *cache.Cache
	tm       int
	duration time.Duration
	EChan    chan *SgKV[T]
}

type SgKV[T any] struct {
	K string
	V []T
}

// tm 聚合的时间间隔， tt 聚合的时间类型，秒、分钟、小时
func NewSgAggr[T any](tm int, tt time.Duration) *SgTimeAggr[T] {
	a := &SgTimeAggr[T]{
		c:        cache.New(time.Duration(10)*time.Second, 2*time.Second),
		tm:       tm,
		duration: tt,
		EChan:    make(chan *SgKV[T], 10),
	}
	a.c.OnEvicted(func(s string, i interface{}) {
		ds := i.([]T)
		kv := &SgKV[T]{K: s, V: ds}
		a.EChan <- kv
	})
	return a
}

func NewSgAggr3[T any](tm int, tt time.Duration, timeout int) *SgTimeAggr[T] {
	a := &SgTimeAggr[T]{
		c:        cache.New(time.Duration(timeout)*time.Second, 2*time.Second),
		tm:       tm,
		duration: tt,
		EChan:    make(chan *SgKV[T], 10),
	}
	a.c.OnEvicted(func(s string, i interface{}) {
		ds := i.([]T)
		kv := &SgKV[T]{K: s, V: ds}
		a.EChan <- kv
	})
	return a
}

func (a *SgTimeAggr[T]) Add(prefix string, dt int64, v T) {
	k := a.newKey(prefix, dt)
	// fmt.Println("key", k)
	lst, b := a.c.Get(k)
	if !b {
		lst = make([]T, 0, 300)
	}
	sl := lst.([]T)
	sl = append(sl, v)
	a.c.SetDefault(k, sl)
}
func (a *SgTimeAggr[T]) Add1(dt int64, v T) {
	k := a.newKey1(dt)
	// fmt.Println("key", k)
	lst, b := a.c.Get(k)
	if !b {
		lst = make([]T, 0, 300)
	}
	sl := lst.([]T)
	sl = append(sl, v)
	a.c.SetDefault(k, sl)
}

func (a *SgTimeAggr[T]) Size() int {
	return len(a.c.Items())
}

func (a *SgTimeAggr[T]) newKey(prefix string, dt int64) string {
	m := 0
	tm :=
		time.UnixMilli(dt)
	switch a.duration {
	case time.Second:
		m = tm.Second()
	case time.Minute:
		m = tm.Minute()
	case time.Hour:
		m = tm.Hour()
	}
	m = m / a.tm * a.tm
	return prefix + "_" + strconv.Itoa(m)
}
func (a *SgTimeAggr[T]) newKey1(dt int64) string {
	m := 0
	tm :=
		time.UnixMilli(dt)
	switch a.duration {
	case time.Second:
		m = tm.Second()
	case time.Minute:
		m = tm.Minute()
	case time.Hour:
		m = tm.Hour()
	}
	m = m / a.tm * a.tm
	return "_" + strconv.Itoa(m)
}
