package sgmsg

import (
	"log/slog"
	"time"

	"nanomsg.org/go/mangos/v2/protocol/pull"
	"nanomsg.org/go/mangos/v2/protocol/push"

	// register transports
	_ "nanomsg.org/go/mangos/v2/transport/ipc"
)

type (
	SgPush struct {
		SgSender sgSender
	}
	SgPull struct {
		SgAccepter sgAccepter
	}
)

func NewSgPush() *SgPush {
	return &SgPush{
		SgSender: sgSender{},
	}
}

func (n *SgPush) Start(url string) *SgPush {
	var err error
	if n.SgSender.sock, err = push.NewSocket(); err != nil {
		slog.Error("nnmsg.go", "Start SgPush", err)
	}
	go n.dial(url)
	return n
}

func (n *SgPush) dial(url string) {
	for {
		if err := n.SgSender.sock.Dial(url); err != nil {
			slog.Error("nnmsg.go", "dial", "url", url, "err", err)
			time.Sleep(time.Second * 1)
		} else {
			slog.Info("dial=========成功！======", "url", url)
			return
		}
	}
}

func NewSgPull() *SgPull {
	return &SgPull{
		SgAccepter: sgAccepter{
			handlerFuncs: make(map[string]SgResFunc),
		},
	}
}

func (s *SgPull) Start(url string) {
	var err error

	s.SgAccepter.sock, err = pull.NewSocket()
	if err != nil {
		slog.Error("nnmsg.go", "Start SgPull", err)
	}

	go s.SgAccepter.listening(url)
}
