package sgmsg

import (
	"log/slog"
	"time"

	"gitee.com/sgems/sg.core/utils"
	"nanomsg.org/go/mangos/v2"
)

type SgResFunc func(resp JsonEntity)

type (
	Resource struct {
		Path []byte
		Data []byte
	}
	JsonResource struct {
		Path string
		Data []byte
	}
	sgSender struct {
		sock mangos.Socket
	}
	sgAccepter struct {
		sock         mangos.Socket
		handlerFuncs map[string]SgResFunc
	}

	JsonEntity struct {
		Dt   int64
		Attm time.Time
		Tp   int //数据类型
		Addr int //设备地址
		Jn   []byte
	}
)

func (n *sgSender) SendJsonResource(path string, data *JsonEntity) {
	res := JsonResource{Path: path, Data: utils.ToJsonBytes(data)}
	if err := n.sock.Send(utils.ToJsonBytes(res)); err != nil {
		slog.Error("nnmsg.go", "SendMsg", "err", err)
	}
}

func (n *sgSender) SendEmsResource(path string, tp int, no int, data any) {
	jne := OfJsonEntity(tp, no, data)
	res := JsonResource{Path: path, Data: utils.ToJsonBytes(jne)}
	if err := n.sock.Send(utils.ToJsonBytes(res)); err != nil {
		slog.Error("nnmsg.go", "SendMsg", "err", err)
	}
}

func (n *sgSender) Close() { n.sock.Close() }

func (s *sgAccepter) RegHandler(path string, f SgResFunc) *sgAccepter {
	s.handlerFuncs[path] = f
	return s
}
func (s *sgAccepter) listening(url string) {
	if err := s.sock.Listen(url); err != nil {
		slog.Error("msgbus: error listening on ", "url", url, "err", err)
	}
	for {
		msg, _ := s.sock.Recv()
		res, b := utils.FromJsonBytes[JsonResource](msg)
		if b && s.handlerFuncs[res.Path] != nil {
			jne, _ := utils.FromJsonBytes[JsonEntity](res.Data)
			s.handlerFuncs[res.Path](jne)
		}
	}
}

func (s *sgAccepter) Dialing(url string) {
	for {
		time.Sleep(time.Second * 1)
		if err := s.sock.Dial(url); err != nil {
			slog.Error("msgbus: error listening on ", "url", url, "err", err)
			continue
		}

		if err := s.sock.SetOption(mangos.OptionSubscribe, []byte("")); err != nil {
			panic(err)
		}
		return
	}

}

func (s *sgAccepter) Recv() {
	for {
		msg, _ := s.sock.Recv()
		res, b := utils.FromJsonBytes[JsonResource](msg)
		if b && s.handlerFuncs[res.Path] != nil {
			jne, _ := utils.FromJsonBytes[JsonEntity](res.Data)
			s.handlerFuncs[res.Path](jne)
		}
	}
}

func (s *sgAccepter) Stop() { s.sock.Close() }

func OfJsonEntity(tp int, addr int, data any) *JsonEntity {
	jn := utils.ToJsonBytes(data)
	tm := time.Now()
	return &JsonEntity{
		Tp:   tp,
		Addr: addr,
		Jn:   jn,
		Dt:   tm.UnixMilli(),
		Attm: tm,
	}
}
