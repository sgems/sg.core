package sgmsg

import (
	"log/slog"
	"time"

	"nanomsg.org/go/mangos/v2/protocol/pub"
	"nanomsg.org/go/mangos/v2/protocol/sub"

	// register transports
	_ "nanomsg.org/go/mangos/v2/transport/ipc"
)

type (
	SgPub struct {
		SgSender sgSender
	}
	SgSub struct {
		SgAccepter sgAccepter
	}
)

func NewSgPub() *SgPub {
	return &SgPub{
		SgSender: sgSender{},
	}
}

func (n *SgPub) Start(url string) *SgPub {
	var err error

	if n.SgSender.sock, err = pub.NewSocket(); err != nil {
		slog.Error("nnmsg.go", "Start SgPush", err)
	}
	go n.dial(url)
	return n
}

func (n *SgPub) dial(url string) {
	for {
		if err := n.SgSender.sock.Listen(url); err != nil {
			slog.Error("nnmsg.go", "dial", "url", url, "err", err)
			time.Sleep(time.Second * 1)
		} else {
			slog.Info("dial=========成功！======", "url", url)
			return
		}
	}
}

func NewSgSub() *SgSub {
	return &SgSub{
		SgAccepter: sgAccepter{
			handlerFuncs: make(map[string]SgResFunc),
		},
	}
}

func (s *SgSub) Start(url string) {
	var err error

	s.SgAccepter.sock, err = sub.NewSocket()
	if err != nil {
		slog.Error("nnmsg.go", "Start SgPull", err)
	}

	go s.SgAccepter.Dialing(url)
	go s.SgAccepter.Recv()
}
