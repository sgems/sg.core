module gitee.com/sgems/sg.core

go 1.21.1

require (
	github.com/eclipse/paho.mqtt.golang v1.4.3
	github.com/patrickmn/go-cache v2.1.0+incompatible
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	github.com/Microsoft/go-winio v0.4.11 // indirect
	golang.org/x/sys v0.6.0 // indirect
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	nanomsg.org/go/mangos/v2 v2.0.8
)
